module.exports = [
	{
		id: 1,
		name: 'Beach',
		status: 'Available',
		description:'Paint of Beach using oil paints',
		image: require('../images/monet/beach.jpeg')
	},
	{
		id: 2,
		name: 'Art connect',
		status: 'Sold',
		description:'Paint of art using oil paints',
		image: require('../images/splash.png')
	},
	{
		id: 3,
		name: 'Bridge',
		status: 'Available',
		description:'Paint of Bridge using oil paints',
		image: require('../images/monet/bridge.jpeg')
	},
	{
		id: 4,
		name: 'Cliff',
		status: 'Available',
		description:'Paint of Cliff using oil paints',
		image: require('../images/monet/cliff.jpeg')
	},
	{
		id: 5,
		name: 'Mountian',
		status: 'Sold',
		description:'Paint of Mountian using oil paints',
		image: require('../images/monet/mountain.jpeg')
	},
	{
		id: 6,
		name: 'River Boats',
		status: 'Available',
		description:'Paint of Beach using oil paints',
		image: require('../images/monet/river-boats.jpeg')
    },
    
];    