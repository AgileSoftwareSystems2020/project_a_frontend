// monet uri's
export default [
  {
    "name": "beach",
    "uri": require('./images/monet/beach.jpeg')
  },
  {
    "name": "bridge",
    "uri": require('./images/monet/bridge.jpeg')
  },
  {
    "name": "cliff",
    "uri": require('./images/monet/cliff.jpeg')
  },
  {
    "name": "mountain",
    "uri": require('./images/monet/mountain.jpeg')
  },
  {
    "name": "river-boats",
    "uri": require('./images/monet/river-boats.jpeg')
  }
]