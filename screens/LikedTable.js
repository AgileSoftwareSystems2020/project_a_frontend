import React, { Component } from 'react';
import { StyleSheet, View, ScrollView } from 'react-native';
import { Table, TableWrapper, Row } from 'react-native-table-component';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tableHead: ['First', 'Last','Message'],
      widthArr: [60,60,350]
    }
  }

  render() {
    const state = this.state;
    const data = [];
    const dataRow1 = [
      ["Alan"],["Smith"],["Is this painting still avalible?"]
    ];
    data.push(dataRow1);
    const dataRow2 = [
      ["John"],["Brown"],["How much?"]
    ];
    data.push(dataRow2);
    const dataRow3 = [
      ["Rachel"],["Hooper"],["I really like your painting."]
    ];
    data.push(dataRow3);
    const dataRow4 = [
      ["Bob"],["Seger"],["Great Painting!"]
    ];
    data.push(dataRow4);
    const dataRow5 = [
      ["Terra"],["Johnson"],["Awesome Work!"]
    ];
    data.push(dataRow5);
    const dataRow6 = [
      ["Mike"],["Hank"],["Is 100$ okay?"]
    ];
    data.push(dataRow6);
    const dataRow7 = [
      ["Jessie"],["White"],["What is your best offer?"]
    ];
    data.push(dataRow7);
    const dataRow8 = [
      ["Gorge"],["Harison"],["What kind of paint is it?"]
    ];
    data.push(dataRow8);
    const dataRow9 = [
      ["Minny"],["Monet"],["Can you make a better offer?"]
    ];
    data.push(dataRow9);

    return (
      <View style={styles.container}>
        <ScrollView horizontal={true}>
          <View>
            <Table borderStyle={{borderColor: '#C1C0B9'}}>
              <Row data={state.tableHead} widthArr={state.widthArr} style={styles.head} textStyle={styles.text}/>
            </Table>
            <ScrollView style={styles.dataWrapper}>
              <Table borderStyle={{borderColor: '#C1C0B9'}}>
                {
                  data.map((dataRow, index) => (
                    <Row
                      key={index}
                      data={dataRow}
                      widthArr={state.widthArr}
                      style={[styles.row, index%2 && {backgroundColor: '#ffffff'}]}
                      textStyle={styles.text}
                    />
                  ))
                }
              </Table>
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: { 
    flex: 1, 
    padding: 16, 
    paddingTop: 30, 
    backgroundColor: '#ffffff' 
  },
  head: { 
    height: 50, 
    backgroundColor: '#bb1111' 
  },
  text: { 
    textAlign: 'center', 
    fontWeight: '700' 
  },
  dataWrapper: { 
    marginTop: -1 
  },
  row: { 
    height: 100, 
    backgroundColor: '#F7F8FA' 
  }
});