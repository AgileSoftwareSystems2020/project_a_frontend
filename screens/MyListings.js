import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Button, Text } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { TouchableOpacity } from 'react-native';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tableHead: ['Art Name', 'Date Listed', 'Likes'],
      widthArr: [120, 120, 80]
    }
  }

  

  render() {
    
    const state = this.state;
    const data = [
      ["The sea, a portrait","April 17, 2020", 5],
      ["Horses in Motion", "December 1, 2019", 0],
      ["The Color of Jazz", "March 3, 2020", 13]
    ];
    
    
    return (
      <View style={styles.container}>
        <Listing artName="The Sea" artLikes="5" screenName="seaListing"></Listing>
        <Text></Text>
        <ListingAlt artName="A Horse" artLikes="1" screenName="horsesListing"></ListingAlt>
        <Text></Text>
        <Listing artName="Jazz #8" artLikes="9" screenName="jazzListing"></Listing>
        <Text></Text>
        <Text></Text>
        <Button title="+New Listing"></Button>
      </View>
    )

  }
  
}

function Listing({artName, artLikes, screenName}) {
  const navigation = useNavigation();
  return (
    <TouchableOpacity onPress={() => navigation.navigate(screenName)}> 
      <View style={styles.listing}>
      <Text style={styles.text}>{artName}          Likes: {artLikes}</Text>
      </View>
    </TouchableOpacity>
  );
}

function ListingAlt({artName, artLikes, screenName}) {
  const navigation = useNavigation();
  return (
    <TouchableOpacity onPress={() => navigation.navigate(screenName)}> 
      <View style={styles.listingAlt}>
      <Text style={styles.text}>{artName}          Likes: {artLikes}</Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: { 
    flex: 1, 
    padding: 16, 
    paddingTop: 30, 
    backgroundColor: '#ffffff' 
  },
  listing: { 
    height: 50, 
    backgroundColor: '#FF6F61',
    
    fontSize: 40,
    
  },
  listingAlt: { 
    height: 50, 
    backgroundColor: '#DC143C',
    
    fontSize: 40,
    
  },
  head: { 
    height: 50, 
    backgroundColor: '#bb1111' 
  },
  text: { 
    fontSize: 30,
    fontWeight: '400' 
  },
  dataWrapper: { 
    marginTop: -1 
  },
  row: { 
    height: 100, 
    backgroundColor: '#F7F8FA' 
  }
});