import React from 'react';
import { StyleSheet, Text, View, Image, TextInput, Button,ScrollView } from 'react-native';

export default function App({navigation}) {
  return (
    <ScrollView>
    <View style={styles.container}>
      <View>
      <Image style={styles.image} source={require('../assets/images/artconnectlogo.png')} />
      </View>
      <View >
        <Text style={styles.boldText} >Email</Text>
      <TextInput style={styles.textInput}/>
        <Text style={styles.boldText}>Reason</Text>
      <TextInput style={styles.textInput}/>
        <Text style={styles.boldText}>Subject</Text>
      <TextInput style={styles.textInput}/>
        <Text style={styles.boldText}>Message</Text>
      <TextInput style={styles.textInput2}/>
      </View>
      <Button
        onPress={() => {
        alert('Your message has been sent!');
        }}
        title="Send"
  />   
    </View>
    </ScrollView>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  body: {
    padding: 50,
    alignItems:'center',
    justifyContent:'center',
  },
  boldText: {
    // fontWeight: 'bold',
    fontSize: 30
  },
  image: {
    aspectRatio: 1.0, 
    resizeMode: 'contain',
  },
  textInput: {
    height: 40,
    borderColor: 'red',
    borderWidth: 1,
    width: 200
  },
  textInput2: {
    height: 150,
    borderColor: 'red',
    borderWidth: 1,
    width: 200
  },
  button: {
    borderWidth: 5,
    color: 'red',

  },
});