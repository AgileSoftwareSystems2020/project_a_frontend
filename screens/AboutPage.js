import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.body}>
        <Text style={styles.boldText}>Our Mission</Text>
        <Text>Our goal is to get local artist to get in touch with local art creaters, while leaving out the exclusivity of the art buying and selling world.
        </Text>
      </View>
      <View>
      <Image style={styles.image} source={require('../assets/images/artconnectlogo.png')} />
      </View>
      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  body: {
    padding: 50,
    alignItems:'center',
    justifyContent:'center',
  },
  boldText: {
    fontWeight: 'bold',
  },
  image: {
    aspectRatio: 1.0, 
    resizeMode: 'contain',
  }
});