import React from 'react';
import { StyleSheet, Text, View, Image, TextInput, Button } from 'react-native';
import { useNavigation } from '@react-navigation/native';





export default function App({navigation}) {
  return (
    
    <View style={styles.container}>
      <GoToButton screenName='UpdateEmail' buttonTitle='update email address'/>
      <Text></Text>
      <GoToButton screenName='UpdatePassword' buttonTitle='update password'/>
      <Text></Text>
      <GoToButton screenName='UpdatePayment' buttonTitle='update payment'/>
      <Text></Text>
      <GoToButton screenName='Create' buttonTitle='Delete Account'/>
    </View>
  );
}


function GoToButton({ screenName, buttonTitle }) {
  const navigation = useNavigation();
  return (
    <Button
      title={buttonTitle}
      onPress={() => navigation.navigate(screenName)}
    />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    padding:50
  },
  body: {
    padding: 50,
    alignItems:'center',
    justifyContent:'center',
  },
  boldText: {
    // fontWeight: 'bold',
    fontSize: 30
  },
  image: {
    aspectRatio: 1.0, 
    resizeMode: 'contain',
  },
  textInput: {
    height: 40,
    borderColor: 'red',
    borderWidth: 1,
    width: 200
  },
  button: {
    borderWidth: 5,
    color: 'red',

  },
});