import React from 'react';
import { StyleSheet, Text, View, Image, TextInput, Button } from 'react-native';
import { useNavigation } from '@react-navigation/native';





export default function App({navigation}) {
  return (
    
    <View style={styles.container}>
      <View>
      <Image style={styles.image} source={require('../assets/images/artconnectlogo.png')} />
      </View>
      <View >
        <Text style={styles.boldText} >Email</Text>
      <TextInput style={styles.textInput}/>
        <Text style={styles.boldText}>Password</Text>
      <TextInput style={styles.textInput}/>
      <GoToButton screenName='MyListings'/> 
      {/* above button needs to go to myListings */}
      </View>
      
      
    </View>
  );
}


function GoToButton({ screenName }) {
  const navigation = useNavigation();
  return (
    <Button
      title='Submit'
      onPress={() => navigation.navigate(screenName)}
    />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  body: {
    padding: 50,
    alignItems:'center',
    justifyContent:'center',
  },
  boldText: {
    // fontWeight: 'bold',
    fontSize: 30
  },
  image: {
    aspectRatio: 1.0, 
    resizeMode: 'contain',
  },
  textInput: {
    height: 40,
    borderColor: 'red',
    borderWidth: 1,
    width: 200
  },
  button: {
    borderWidth: 5,
    color: 'red',

  },
});