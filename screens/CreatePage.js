import React from 'react';
import { StyleSheet, Text, View, Image, TextInput, Button, ScrollView } from 'react-native';
import { useNavigation } from '@react-navigation/native';





export default function App({navigation}) {
  return (
    <ScrollView>
      <View style={styles.container}>
        <View>
        <Image style={styles.image} source={require('../assets/images/artconnectlogo.png')} />
        </View>
        <View >
          <Text style={styles.boldText} >First Name</Text>
        <TextInput style={styles.textInput}/>
          <Text style={styles.boldText} >Last Name</Text>
        <TextInput style={styles.textInput}/>
          <Text style={styles.boldText} >Age</Text>
        <TextInput style={styles.textInput}/>
          <Text style={styles.boldText} >Email</Text>
        <TextInput style={styles.textInput}/>
          <Text style={styles.boldText}>Password</Text>
        <TextInput style={styles.textInput}/>
          <Text style={styles.boldText} >Comfirm Password</Text>
        <TextInput style={styles.textInput}/>
          <Text style={styles.boldText} >Type of Account</Text>
        <GoToButton1 screen1='Buyer' style={styles.button}/>
        <GoToButton2 screen2='Seller' style={styles.button}/>
        </View>
      </View>
    </ScrollView>
  );
}


function GoToButton1({ screen1 }) {
  const navigation = useNavigation();
  return (
    <Button
      title='Buyer'
      onPress={() => navigation.navigate(screen1)}
    />
  );
}
function GoToButton2({screen2 }) {
  const navigation = useNavigation();
  return (
    <Button
      title='Seller'
      onPress={() => navigation.navigate(screen2)}
    />
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    padding: 50,
    width: 5
  },
  body: {
    padding: 50,
    alignItems:'center',
    justifyContent:'center',
  },
  boldText: {
    // fontWeight: 'bold',
    fontSize: 30
  },
  image: {
    aspectRatio: 1.0, 
    resizeMode: 'contain',
  },
  textInput: {
    height: 40,
    borderColor: 'red',
    borderWidth: 1,
    width: 200
  },
  button: {
    display: 'flex',
    borderWidth: 5,
    color: 'red',

  },
});