import React from 'react';
import { StyleSheet, Text, View, Image, TextInput, Button } from 'react-native';
import { useNavigation } from '@react-navigation/native';





export default function App({navigation}) {
  return (
    
    <View style={styles.container}>
      <Text>Old Email</Text>
      <TextInput style={styles.textInput}/>
      <Text>New Email</Text>  
      <TextInput style={styles.textInput}/>
      <Text>Confirm New Email</Text>
      <TextInput style={styles.textInput}/>
      <Button
        title="Submit"
        onPress={() => navigation.goBack()}
      />
    </View>
  );
}


function GoToButton({ screenName }) {
  const navigation = useNavigation();
  return (
    <Button
      title='Submit'
      onPress={() => navigation.navigate(screenName)}
    />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  body: {
    padding: 50,
    alignItems:'center',
    justifyContent:'center',
  },
  boldText: {
    // fontWeight: 'bold',
    fontSize: 30
  },
  image: {
    aspectRatio: 1.0, 
    resizeMode: 'contain',
  },
  textInput: {
    height: 40,
    borderColor: 'red',
    borderWidth: 1,
    width: 200
  },
  button: {
    borderWidth: 5,
    color: 'red',

  },
});