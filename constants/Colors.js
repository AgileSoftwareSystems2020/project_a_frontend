const tintColor = '#2f95dc';

export default {
  tintColor,
  tabIconDefault: 'black',
  tabIconSelected: 'red',
  tabBar: '#385454',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
};
