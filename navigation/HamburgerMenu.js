import * as React from 'react';
import DrawerIcon from '../components/DrawerIcon';
import { createDrawerNavigator } from '@react-navigation/drawer';
import FreshListings from '../screens/FreshListings';
import LikedListings from '../screens/LikedListings';
import AboutPage from '../screens/AboutPage';
import BuyerLogin from '../screens/BuyerLogin';
import CreatePage from '../screens/CreatePage';
import SellerLogin from '../screens/SellerLogin';
import ContactPage from '../screens/ContactPage';
import MyListings from '../screens/MyListings';
import theSeaListing from '../screens/ListingPages/theSeaListing';
import jazzListing from '../screens/ListingPages/jazzListing';
import horsesListing from '../screens/ListingPages/horsesListing';
import AccountManagement from '../screens/AccountManagement';
import UpdateEmail from '../screens/AccountManagement/UpdateEmail';
import UpdatePassword from '../screens/AccountManagement/UpdatePassword';
import UpdatePayment from '../screens/AccountManagement/UpdatePayment';
import LikedTable from '../screens/LikedTable';
const Drawer = createDrawerNavigator();
const INITIAL_ROUTE_NAME = 'Create';

export default function HamBurgerMenu({ navigation, route }) {
    navigation.setOptions({ headerTitle: getHeaderTitle(route) });
    return (
        <Drawer.Navigator initialRouteName={INITIAL_ROUTE_NAME}>
            <Drawer.Screen
                name="Fresh"
                component={FreshListings}
                options={{
                    title: 'Fresh Art',
                    drawerIcon: ({focused}) => <DrawerIcon focused={focused} name="ios-brush" />,
                }}
            />
            <Drawer.Screen
                name="Liked"
                component={LikedListings}
                options={{
                    title: 'Liked Art',
                    drawerIcon: ({focused}) => <DrawerIcon focused={focused} name="ios-thumbs-up" />,
                }}
            />
            <Drawer.Screen
                name="MyListings"
                component={MyListings}
                options={{
                    drawerIcon: ({focused}) => <DrawerIcon focused={focused} name="ios-albums" />,
                }}    
            />
            <Drawer.Screen
                name="Offers"
                component={LikedTable}
                options={{
                    drawerIcon: ({focused}) => <DrawerIcon focused={focused} name="ios-list-box" />,
                }}  
            />
            <Drawer.Screen
                name="Create"
                component={CreatePage}
                options={{
                    drawerIcon: ({focused}) => <DrawerIcon focused={focused} name="ios-add-circle" />,
                }}
            />
            <Drawer.Screen
                name="Buyer"
                component={BuyerLogin}
                options={{
                    drawerIcon: ({focused}) => <DrawerIcon focused={focused} name="ios-pricetags" />,
                }} 
            />
            <Drawer.Screen
                name="Seller"
                component={SellerLogin}
                options={{
                    drawerIcon: ({focused}) => <DrawerIcon focused={focused} name="ios-cash" />,
                }} 
            />
            <Drawer.Screen
                name="AccountManagement"
                component={AccountManagement}
                options={{
                    title: 'Manage Your Account',
                    drawerIcon: ({focused}) => <DrawerIcon focused={focused} name="ios-cog" />,
                }}
            />
            <Drawer.Screen
                name="Contact"
                component={ContactPage}
                options={{
                    drawerIcon: ({focused}) => <DrawerIcon focused={focused} name="ios-mail" />,
                }}  
            />
        
            <Drawer.Screen
                name="About"
                component={AboutPage}
                options={{
                    title: 'About Us',
                    drawerIcon: ({focused}) => <DrawerIcon focused={focused} name="ios-people" />,
                }}
            />

             <Drawer.Screen
                name="UpdateEmail"
                component={UpdateEmail}
            />
            <Drawer.Screen
                name="UpdatePayment"
                component={UpdatePayment}
            />
            <Drawer.Screen
                name="UpdatePassword"
                component={UpdatePassword}
            />
            <Drawer.Screen
                name="seaListing"
                component={theSeaListing}
            />
            <Drawer.Screen
                name="horsesListing"
                component={horsesListing}
            />
            <Drawer.Screen
                name="jazzListing"
                component={jazzListing}
            />
        </Drawer.Navigator>
    );
}
function getHeaderTitle(route) {
    const routeName = route.state?.routes[route.state.index]?.name ?? INITIAL_ROUTE_NAME;
    switch (routeName) {
    case 'Fresh':
        return 'Fresh Art';
    case 'Liked':
        return 'Liked Art';
    case 'About':
        return 'About Us';
    case 'Create':
        return 'Create Account';
    case 'Buyer':
        return 'Buyer Login';
    case 'Seller':
        return 'Seller Login';
    case 'Contact':
        return 'Contact Us';
    case 'Offers':
        return 'Art Offers';
    case 'MyListings':
        return 'My Listings';
    case 'AccountManagement':
        return 'Account Management';
    case 'UpdateEmail':
        return 'Update Email';
    case 'UpdatePayment':
        return 'Update Payment';
    case 'UpdatePassword':
        return 'Update Password';
    case 'seaListing':
        return 'Sea';
    case 'horsesListing':
        return 'Horses';
    case 'jazzListing':
        return 'Jazz';
    }
  }